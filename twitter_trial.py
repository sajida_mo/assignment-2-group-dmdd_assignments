# -*- coding: utf-8 -*-
"""
Created on Tue Feb 26 21:20:26 2019

@author: msaji
"""

import tweepy
import pandas as pd
from scipy.misc import imread

#Authentication

CONSUMER_KEY = '52Q9kmliFexOgDFZNJAW55Ni5'
CONSUMER_SECRET = '1l2vvINHmlkCaMH5UBd7XO2SACa4ANdLxaB56xagjYDK1hyZN2'
OAUTH_TOKEN = '1089601686887432197-vBB5XDXHf1aawGL9yHjg9fSD0pOKqu'
OAUTH_TOKEN_SECRET = 'RbDACKNZbFWZ4tx43YQRJQ5gxRwplwvud8jtL38yMIReq'

auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET) #Interacting with twitter's API
auth.set_access_token(OAUTH_TOKEN, OAUTH_TOKEN_SECRET)
tweepy_api = tweepy.API (auth, wait_on_rate_limit=True) #creating the API object


#Extracting Tweets
results = []
for q in ['Luther', 'Justified', 'Insidious']:
    for tweet in tweepy.Cursor (tweepy_api.search,q, lang = "en").items(2000): 
            results.append(tweet)
    
def tweets_df(results):
    id_list = [tweet.id for tweet  in results]
    data_set = pd.DataFrame(id_list, columns = ["tweet_id"])
    
    data_set["text"] = [tweet.text for tweet in results]
    data_set["user_id"] = [tweet.user.id for tweet in results]
    data_set["user_name"] = [tweet.user.name for tweet in results]
    data_set["created_at"] = [tweet.created_at for tweet in results]
    data_set["favorite_count"] = [tweet.favorite_count for tweet in results]
    data_set["retweet_count"] = [tweet.retweet_count for tweet in results]
    data_set["user_screen_name"] = [tweet.author.screen_name for tweet in results]
    data_set["user_followers_count"] = [tweet.author.followers_count for tweet in results]
    data_set["user_location"] = [tweet.author.location  for tweet in results]
    data_set["hashtags"] = [tweet.entities['hashtags'] for tweet in results]
    
    return data_set
data_set = tweets_df(results)


# Remove tweets with duplicate text
text = data_set["text"]

for i in range(0,len(text)):
    txt = ' '.join(word for word in text[i] .split() if not word.startswith('https:'))
    data_set.set_value(i, 'text2', txt)
    
data_set.drop_duplicates('text2', inplace=True)
data_set.reset_index(drop = True, inplace=True)
data_set.drop('text', axis = 1, inplace = True)
data_set.rename(columns={'text2': 'text'}, inplace=True)
data_set.to_csv('Tweet_data.csv')