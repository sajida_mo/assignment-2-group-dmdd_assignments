# -*- coding: utf-8 -*-
"""
code adapted from Dataquest
Created on Sun Jan 27 14:18:24 2019

@author: msaji

"""
from requests import get
from time import sleep
from time import time
from random import randint
from warnings import warn

import pandas as pd

from IPython.core.display import clear_output

from bs4 import BeautifulSoup as bs

headers = {"Accept-Language": "en-US, en;q=0.5"}
pages = [str(i) for i in range(1,3600, 50)]

#to handle requests being sent
start_time = time()
requests = 0

#Declaring the variables to be used for data scraped

series_names = []
year_release = []
imdb_ratings = []
certificate = []
metascores = []
votes = []
movie_description = []
certificate = []
runtime = []
genre = []
director_name = []
star_cast = []
    

for page in pages:
    
    response = get('http://www.imdb.com/search/title?release_date=2010-01-01,2010-12-31'+ '&start=' + page +'&ref_=adv_nxt',
                   headers = headers)

    # Pause the loop
    sleep(randint(8,15))

    # Monitor the requests
    requests += 1
    elapsed_time = time() - start_time
    print('Request:{}; Frequency: {} requests/s'.format(requests, requests/elapsed_time))
    clear_output(wait = True)

    # Throw a warning for non-200 status codes
    if response.status_code != 200:
        warn('Request: {}; Status code: {}'.format(requests, response.status_code))

    # Break the loop if the number of requests is greater than expected
    if requests > 72:
        warn('Number of requests was greater than expected.')  
        break 
    ## importing Beautiful Soup
    html_soup = bs(response.text,'html.parser')## python's in built library HTML parser

    id_check = html_soup.find(id ="main")
    movie_container = id_check.find_all(class_ ="lister-item mode-advanced")


    container = movie_container[0]
    meta = container.find("span", class_ = "userRatingValue").text


    for container in movie_container:

        #the movie_name
        name = container.h3.a.text
        series_names.append(name)
        
        #the year od release
        release = container.find("span", class_ = "lister-item-year text-muted unbold").text
        year_release.append(str(release).replace('(I)', '').replace("-",''))
        
        #the ratings for the movies
        if container.strong:
            ratings = float(container.strong.text)
            imdb_ratings.append(int(ratings)*10)
        else:
            imdb_ratings.append("NA")

        
        #the votes
        if  container.find("span", attrs = {"name":"nv"}):
            vote = container.find("span", attrs = {"name":"nv"})['data-value']
            votes.append(int(vote))
        else:
            votes.append(0)
        
        #the runtime
        if container.find("span", class_ ="runtime"):
            run = container.find("span", class_ ="runtime").text
            runtime.append(str(run).replace('min', ''))           
        else:
            runtime.append("NA")
        #the genre
        if container.find("span", class_ ="genre"):
            gen = container.find("span", class_ ="genre").text
            genre.append(str(gen).strip(' ').strip('\n'))
        else:
            genre.append("NA")
        
        #the certificate
        if container.find("span", class_ ="certificate"):
            cer = container.find("span", class_ ="certificate").text
            certificate.append(str(cer))
        else:
            certificate.append("NA")
        
        #fetching all <p> tags
        content = container.find_all("p")
        
        #the description
        desc = content[1].text
        movie_description.append(str(desc).strip(' ').strip('\n    ').replace('               See full summary\xa0»','').replace('...',''))
        
        #subsetting all the <a> tags in 3rd <p> tag
        content_2 = content[2].find_all("a")
        
                
       #extracting artists names
        temp = []
        for i in range(len(content_2)-1):
            temp.append(content_2[i].text)
        star_cast.append(temp)
        


test_df = pd.DataFrame({"Series_names":series_names,
                        "year_release":year_release,
                        "imdb_ratings":imdb_ratings,
                        "runtime":runtime,
                        "votes":votes,
                        "movie_description":movie_description,
                        "certificate":certificate,
                        "genre":genre,
                        "star_cast": star_cast,
                        })
test_df.to_csv(r'C:\Users\msaji\OneDrive\Desktop\DMDD\Assignment 2\TV_series_2010.csv')
print("printed")

